# -*- coding: utf-8 -*-
"""
/***************************************************************************
 amsimod_support
                                 A QGIS plugin
 Plugin to support AMSIMOD, an application for process-based modelling
                             -------------------
        begin                : 2016-08-21
        copyright            : (C) 2016 by Angus Carr
        email                : angus.carr@gmail.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load amsimod_support class from file amsimod_support.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .amsimod_support import amsimod_support
    return amsimod_support(iface)
