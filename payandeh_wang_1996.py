"""
Implements equations from Payandeh and Wang, 1996. Variable stocking version of Plonski's yield tables formulated.
Includes table of values...


"""
from math import log as ln
from math import e


class payandeh_wang_1996():
	sp = None
	ht = None
	age = None
	bi = None
	stkg = None
	def __init__(self,leadsp,topht,age,bi,stkg):
		if leadsp in [ 'PB', 'PO', 'PET', 'PEB' ]:
			self.sp = 'PT' # 'Tolerant hardwood' has no stemdensity parameter...
		elif leadsp in [ 'SW','LA', 'BF' ,'EPB','EPN','MEL','SAB']:
			self.sp = 'SB'
		elif leadsp in [ 'CE', 'CW','AB','BOP' ,'THO']:
			self.sp = 'BW'
		elif leadsp in ['PIG']:
			self.sp = 'PJ'
		elif leadsp in ['PEG']:
			self.sp = 'PL'
		else:
			self.sp = leadsp
		self.ht = topht
		self.age = age
		self.bi = bi
		self.stkg = stkg
		
	def return_random_variate(self):
		"""returns a random dbh variate... although it will be uniformly the mean"""
		return self.y(self.sp,'dbh',self.age,self.bi)
		
	def return_random_variates(self,n=10):
		return [ self.y(self.sp,'dbh',self.age,self.bi) for i in range(0,n) ]
		
	def find_bi(self):
		return self.calc_bi(self.sp, self.age, self.ht )
		
	def find_stemsperhectare(self):
		return self.y(self.sp,'stemdensity',self.age,self.bi)
	
	
	def calc_bi(self, sp, age, ht ):
		""" Calcs bi using a golden section search algorithm with no initial upper bound """
		param = self.params[(sp,'ht')]
		b1 = param['b1']
		b2 = param['b2']
		b3 = param['b3']
		b4 = param['b4']
		
		
		bi = 0
		low_bi = None
		high_bi = None
		
		err = None
		
		while high_bi is None:
			#calculate a height at bi - if below height, then keep going, else go up 10 bi units
			temp_ht = self.y(sp,'ht',age,bi)
			#print "bi,temp_ht: %f, %f"%(bi,temp_ht)
			if temp_ht > ht:
				high_bi = bi
			else:
				low_bi = bi
				bi = bi + 10
			assert (bi < 100)
			
			err = abs(temp_ht - ht)
			
		while err > 0.01:
			### have a look between high_bi and low_bi until the err term is less than 10cm
			bi = ( high_bi - low_bi ) / 2.0 + low_bi #go with the midpoint...
			temp_ht = self.y(sp,'ht',age,bi)
			#print "bi,temp_ht: %f, %f"%(bi,temp_ht)
			if temp_ht > ht:
				high_bi = bi
			else:
				low_bi = bi
			#assert (bi < 100)
			
			err = abs(temp_ht - ht)
		
		return bi	
			
		

	def y(self,sp,yield_type,age,bi):
		param = self.params[(sp,yield_type)]
		if param['b4'] is None:
			return self.equn1( param['b1'],param['b2'],param['b3'],bi,age )
		else:
			return self.equn2( param['b1'],param['b2'],param['b3'],param['b4'],bi,age )

	def equn1(self,  b1,b2,b3,bi,t):
		from math import log as ln
		from math import e
		ti = 50 # index age - 50
		p_top = ln( bi / (b1* ( bi ** b2)  )   )
		p =  p_top / ln( 1 - e**( -1 * b3 * ti)  )
		Gt = b1 * (bi ** b2) * (( 1 - e ** ( -1 * b3 * t )) ** p)
		return Gt

	def equn2(self, b1,b2,b3,b4,bi,t):
		from math import log as ln
		from math import e
		try:
			t1 = (bi ** b2)
			t21 = e ** ( -1 * b3 * t )
			t2 = ( 1 - ( t21 ) )** b4 
			return b1 * t1 * t2 # Payendeh and Wang, 1996, Equation 2
		except ValueError as er:
			print er
			return -1
	
	def variable_stocking_yield_table(self,sp,bi):
		""" Use this to check against the article, see if we are getting the same results..."""
		print "age\tdbh\theight\ttree/ha\tba"
		for age in range(20,121,10):
			print "%s\t"%(str(age)),
			print "\t".join( [ str(round(self.y(sp,p,age,bi),1)) for p in ['dbh','ht','stemdensity','ba']]    )
	
	
	
	
	params = {\
		('SB','ht'):	{'sp':'SB',	'yield_type':'ht',	'b1':1.4347,	'b2':0.802,	'b3':0.0203,	'b4':1.6655,	'r':0.99},
		('SB','dbh'):	{'sp':'SB',	'yield_type':'dbh',	'b1':1.9093,	'b2':0.7171,	'b3':0.0164,	'b4':1.4429,	'r':0.99},
		('SB','stemdensity'):	{'sp':'SB',	'yield_type':'stemdensity',	'b1':12815.6823,	'b2':-0.7738,	'b3':0.006,	'b4':-1.0751,	'r':0.99},
		('SB','ba'):	{'sp':'SB',	'yield_type':'ba',	'b1':10.1416,	'b2':0.4256,	'b3':0.0433,	'b4':None,	'r':0.99},
		('SB','vol_main'):	{'sp':'SB',	'yield_type':'vol_main',	'b1':12.9626,	'b2':0.9974,	'b3':0.0267,	'b4':3.3174,	'r':0.99},
		('SB','vol_total'):	{'sp':'SB',	'yield_type':'vol_total',	'b1':11.3908,	'b2':1.1117,	'b3':0.0234,	'b4':3.1272,	'r':0.99},
		('SB','vol_merch'):	{'sp':'SB',	'yield_type':'vol_merch',	'b1':1.2826,	'b2':1.659,	'b3':0.0297,	'b4':8.0576,	'r':0.96},
		('SB','vol_merch_to_7'):	{'sp':'SB',	'yield_type':'vol_merch_to_7',	'b1':5.8019,	'b2':1.2085,	'b3':0.0322,	'b4':5.9513,	'r':0.98},
		('PJ','ht'):	{'sp':'PJ',	'yield_type':'ht',	'b1':0.1633,	'b2':1.5079,	'b3':0.0385,	'b4':1.7461,	'r':0.99},
		('PJ','dbh'):	{'sp':'PJ',	'yield_type':'dbh',	'b1':0.1623,	'b2':1.5253,	'b3':0.0327,	'b4':1.9285,	'r':0.99},
		('PJ','stemdensity'):	{'sp':'PJ',	'yield_type':'stemdensity',	'b1':3410964.73,	'b2':-2.633,	'b3':0.036,	'b4':-3.796,	'r':0.99},
		('PJ','ba'):	{'sp':'PJ',	'yield_type':'ba',	'b1':2.8235,	'b2':0.6944,	'b3':0.063,	'b4':None,	'r':0.99},
		('PJ','vol_main'):	{'sp':'PJ',	'yield_type':'vol_main',	'b1':0.1677,	'b2':2.2609,	'b3':0.0434,	'b4':2.2676,	'r':0.97},
		('PJ','vol_total'):	{'sp':'PJ',	'yield_type':'vol_total',	'b1':0.252,	'b2':2.264,	'b3':0.038,	'b4':2.6547,	'r':0.99},
		('PJ','vol_merch'):	{'sp':'PJ',	'yield_type':'vol_merch',	'b1':0.0377,	'b2':2.6819,	'b3':0.0611,	'b4':7.9121,	'r':0.97},
		('PJ','vol_merch_to_7'):	{'sp':'PJ',	'yield_type':'vol_merch_to_7',	'b1':0.0579,	'b2':2.5602,	'b3':0.0668,	'b4':7.7797,	'r':0.96},
		('PT','ht'):	{'sp':'PT',	'yield_type':'ht',	'b1':0.101,	'b2':1.6809,	'b3':0.0331,	'b4':1.5163,	'r':0.99},
		('PT','dbh'):	{'sp':'PT',	'yield_type':'dbh',	'b1':0.1016,	'b2':1.8012,	'b3':0.0129,	'b4':1.1814,	'r':0.99},
		('PT','stemdensity'):	{'sp':'PT',	'yield_type':'stemdensity',	'b1':1853.194,	'b2':-2.383,	'b3':0.000015,	'b4':-1.0464,	'r':0.99},
		('PT','ba'):	{'sp':'PT',	'yield_type':'ba',	'b1':1.9607,	'b2':0.8651,	'b3':0.042,	'b4':None,	'r':0.99},
		('PT','vol_main'):	{'sp':'PT',	'yield_type':'vol_main',	'b1':0.092,	'b2':2.51,	'b3':0.0421,	'b4':3.4203,	'r':0.99},
		('PT','vol_total'):	{'sp':'PT',	'yield_type':'vol_total',	'b1':0.0518,	'b2':2.7632,	'b3':0.038,	'b4':3.4523,	'r':0.99},
		('PT','vol_merch'):	{'sp':'PT',	'yield_type':'vol_merch',	'b1':0.0333,	'b2':2.7662,	'b3':0.0582,	'b4':8.35,	'r':0.98},
		('PT','vol_merch_to_7'):	{'sp':'PT',	'yield_type':'vol_merch_to_7',	'b1':0.0507,	'b2':2.6739,	'b3':0.0455,	'b4':4.4042,	'r':0.99},
		('BW','ht'):	{'sp':'BW',	'yield_type':'ht',	'b1':0.3468,	'b2':1.31,	'b3':0.0503,	'b4':2.667,	'r':0.99},
		('BW','dbh'):	{'sp':'BW',	'yield_type':'dbh',	'b1':0.0936,	'b2':1.7505,	'b3':0.0347,	'b4':2.4261,	'r':0.99},
		('BW','stemdensity'):	{'sp':'BW',	'yield_type':'stemdensity',	'b1':355005.4518,	'b2':-2.0202,	'b3':0.0242,	'b4':-2.4167,	'r':0.99},
		('BW','ba'):	{'sp':'BW',	'yield_type':'ba',	'b1':1.0759,	'b2':1.021,	'b3':0.0661,	'b4':None,	'r':0.99},
		('BW','vol_main'):	{'sp':'BW',	'yield_type':'vol_main',	'b1':0.4803,	'b2':1.9673,	'b3':0.0574,	'b4':5.0558,	'r':0.99},
		('BW','vol_total'):	{'sp':'BW',	'yield_type':'vol_total',	'b1':0.5205,	'b2':2.0197,	'b3':0.052,	'b4':4.9265,	'r':0.99},
		('BW','vol_merch'):	{'sp':'BW',	'yield_type':'vol_merch',	'b1':0.0108,	'b2':3.0902,	'b3':0.0657,	'b4':11.9262,	'r':0.99},
		('BW','vol_merch_to_7'):	{'sp':'BW',	'yield_type':'vol_merch_to_7',	'b1':0.0623,	'b2':2.5835,	'b3':0.0697,	'b4':12.3547,	'r':0.99},
		('Tolerant hardwood','ht'):	{'sp':'Tolerant hardwood',	'yield_type':'ht',	'b1':1.344,	'b2':0.9523,	'b3':0.0295,	'b4':1.9139,	'r':0.99},
		('Tolerant hardwood','ba'):	{'sp':'Tolerant hardwood',	'yield_type':'ba',	'b1':0.8239,	'b2':1.2802,	'b3':0.0229,	'b4':None,	'r':0.95},
		('Tolerant hardwood','vol_main'):	{'sp':'Tolerant hardwood',	'yield_type':'vol_main',	'b1':7.4984,	'b2':1.3066,	'b3':0.0202,	'b4':2.3999,	'r':0.99},
		('Tolerant hardwood','vol_merch'):	{'sp':'Tolerant hardwood',	'yield_type':'vol_merch',	'b1':3.5345,	'b2':1.4627,	'b3':0.026,	'b4':4.0074,	'r':0.99},
		('PW','ht'):	{'sp':'PW',	'yield_type':'ht',	'b1':0.0987,	'b2':1.5695,	'b3':0.024,	'b4':2.2567,	'r':0.91},
		('PW','ba'):	{'sp':'PW',	'yield_type':'ba',	'b1':4.5074,	'b2':0.609,	'b3':0.0671,	'b4':None,	'r':0.91},
		('PW','vol_main'):	{'sp':'PW',	'yield_type':'vol_main',	'b1':0.4448,	'b2':1.9504,	'b3':0.0242,	'b4':2.4693,	'r':0.9},
		('PW','vol_merch'):	{'sp':'PW',	'yield_type':'vol_merch',	'b1':0.0899,	'b2':2.3574,	'b3':0.0293,	'b4':4.3459,	'r':0.9},
		('PR','ht'):	{'sp':'PR',	'yield_type':'ht',	'b1':0.0244,	'b2':1.8961,	'b3':0.0274,	'b4':1.336,	'r':0.97},
		('PR','ba'):	{'sp':'PR',	'yield_type':'ba',	'b1':8.6021,	'b2':0.4461,	'b3':0.0548,	'b4':None,	'r':0.99},
		('PR','vol_main'):	{'sp':'PR',	'yield_type':'vol_main',	'b1':0.1182,	'b2':2.2674,	'b3':0.0341,	'b4':2.2919,	'r':0.96},
		('PR','vol_merch'):	{'sp':'PR',	'yield_type':'vol_merch',	'b1':0.0091,	'b2':2.9333,	'b3':0.0381,	'b4':3.5955,	'r':0.96},

		}



#	dbh_func(leadsp,topht,age,si,stkg).return_random_variate()
#	def dbh(self,leadsp,topht,age,si,stkg):
		
	
#y(self,sp,yield_type,age,bi):

#(leadsp,topht,age,si,stkg):
#def pw96_yield(leadsp):
#	p = payandeh_wang_1996() 
#	return 	p.dbh

def test():
	p = payandeh_wang_1996()
	print p.params[('SB','ht')]
	
	print p.calc_bi('SB',50,7.5)
	print '=================='
	p.variable_stocking_yield_table('SB',20)
	
	print '=================='
	age = 50
	ht = 8.5 #should be in the 25 zone for bi
	bi = p.calc_bi('SB',age,ht)
	print round(bi,2)
	print round(p.y('SB','stemdensity',age,bi),2)
	print round(p.y('SB','ht',age,bi),2)
	
		
if __name__ == '__main__':
	test()

	
	
	
