#!/usr/bin/python
# cmd  /c C:\OSGeo4W64\osgeo4w.bat  python "%f"
from osgeo import gdal
from osgeo import ogr
import random

from zakrewskiPenner2014 import zp14_equn, zakrewski_penner_equn9
from payandeh_wang_1996 import payandeh_wang_1996

import psycopg2
import math

class generator():
	"""Generates a set of random points, and assigns them attributes 
	according to the parameters provided"""
	parmlist = None # dict() of stand characteristics
	stations = []
	plotline = None
	bufferedoutline = None
	pointlist = None
	treelist = None
	spcomp_list = None
	calc_ba_value = None
	
	def __init__(self,parmlist):
		"""Initializes everything"""
		self.parmlist = parmlist
		
		if 'geom' not in parmlist.keys():
			self.parmlist['geom'] = ogr.CreateGeometryFromWkt(self.parmlist['wkt'])
		
		if 'plot_shape' in parmlist.keys():
			#Using a plotshape - options are "cruise",'square','circular'
			self.make_area_outline(parmlist['plot_area'],parmlist['plot_shape'])
			self.make_spcomp_list()
			self.parmlist['leadsp'] = self.spcomp_list[0]
			self.make_index_list()
			#print self.index_list
			self.parmlist['leadsp_index'] = self.index_list[self.parmlist['leadsp']]
			self.make_stemsperhectare()
			self.make_tree_list()
		else:
			self.make_area_outline('cruise',0)
			self.make_spcomp_list()
			self.parmlist['leadsp'] = self.spcomp_list[0]
			self.make_index_list()
			#print self.index_list
			self.parmlist['leadsp_index'] = self.index_list[self.parmlist['leadsp']]
			self.make_stemsperhectare()


		

	def make_plotline(self,geom=None):
		"""
		# make the plot line from the centroid
		SELECT ST_MakeLine(ST_MakePoint(ST_X($5),ST_Y($5)-90), ST_MakePoint(ST_X($5),ST_Y($5)+90)) as p
		"""
		if geom is None:
			try:
				c = self.parmlist['geom']
			except KeyError: #geom not found... try the wkt item
				self.parmlist['geom'] = ogr.CreateGeometryFromWkt(self.parmlist['wkt'])
				c = self.parmlist['geom']
		else:
			c = geom
		print c.ExportToWkt()
		p = ogr.Geometry(ogr.wkbLineString)
		p.AddPoint(c.GetX(),c.GetY() - 90)
		p.AddPoint(c.GetX(),c.GetY() + 90)
		self.plotline = p
		#print self.plotline.ExportToWkt()
		return self.plotline
	
	def make_stations(self,geom=None):
		"""
		# make stations on the plot line
		SELECT ST_MakePoint( ST_X($5) , ST_Y($5) + generate_series(-90,90,20) ) as geom
		"""
		if geom is None:
			c = self.parmlist['geom']
		else:
			c = geom
		self.stations = [ ogr.CreateGeometryFromWkt('POINT (%f %f)'%(c.GetX(),c.GetY() + y)) for y in range(  -90 , 91 , 20)  ] 
		return self.stations
	
	def make_bufferedoutline(self,distance=40):
		"""
		# buffer the plot line
		SELECT ST_Buffer(p,40) as b
		"""
		#print "minX: %d, minY: %d, maxX: %d, maxY: %d" %(env[0],env[2],env[1],env[3])
		( x0 , x1, y0, y1 )  = self.plotline.Buffer(distance).GetEnvelope()
		wkt = 'POLYGON(( %f %f, %f %f, %f %f, %f %f, %f %f))'%(x0,y0, x0 , y1, x1, y1, x1 , y0 , x0, y0)
		#print wkt
		self.bufferedoutline = ogr.CreateGeometryFromWkt(wkt)
		return self.bufferedoutline
		#print (self.bufferedoutline).ExportToWkt()
		
	def make_area_outline(self,area,shape,geom=None):
		"""Make a plot with a given area in units, a given shape (square | circular)"""
		if geom is None:
			c = self.parmlist['geom']
		else:
			c = geom
			
		if shape == 'square':
			r = math.sqrt(area) / 2  
			( x0 , x1, y0, y1 )  = c.Buffer(r).GetEnvelope()
			wkt = 'POLYGON(( %f %f, %f %f, %f %f, %f %f, %f %f))'%(x0,y0, x0 , y1, x1, y1, x1 , y0 , x0, y0)
			self.bufferedoutline = ogr.CreateGeometryFromWkt(wkt)
			return self.bufferedoutline
		elif shape == 'circular':
			r = math.sqrt(area/math.pi)
			p = c.Buffer(r)
			self.bufferedoutline = p
			return self.bufferedoutline
		elif shape == 'cruise':
			self.make_plotline()
			self.make_stations()
			self.make_bufferedoutline()
			
			
					
	def test(self):
		#print self.plotline.ExportToWkt()
		#print [ s.ExportToWkt() for s in self.stations ]
		#print self.bufferedoutline.ExportToWkt()
		
		# Get forestry parameters ( stems / ha * area of the polygon, dbh distribution function,  )
		#print "Stems / ha: %d"%(self.parmlist['stemsperhectare'])
		msg =  "--Polygon area: %f ha"%(self.bufferedoutline.GetArea() / 10000)
		
		# make points within the polygon
		#print [ s.ExportToWkt() for s in self.randompointsinpolygon() ]
		
		#for n in  [ 'tree: %s, %s, %s'%(t[0].ExportToWkt(),t[1],t[2]) for t in self.make_tree_list() ] :	print n
		if self.parmlist['plot_shape'] == 'cruise':
			msg += '\n'
			msg +=  "--calculate BA: %f"%(self.calc_ba())
		
		return msg
		
		
	## https://pcjericks.github.io/py-gdalogr-cookbook/
	def randompointsinpolygon(self, geom=None, num_points=None, min_distance=0.25):
		if geom is None:
			geom = self.bufferedoutline
		
			
		if num_points is None:
			num_points = self.parmlist['stemsperhectare'] * geom.GetArea() / 10000
			
		
		(minX,maxX,minY,maxY) = geom.GetEnvelope()
		#print "minX: %d, minY: %d, maxX: %d, maxY: %d" %(env[0],env[2],env[1],env[3])
		
		return_set =  []
		return_mp = ogr.Geometry(ogr.wkbMultiPoint)
		i=0
		while len(return_set) < num_points and i < (num_points * 50):
			i = i + 1
			if ( i % 1000) == 0:
				print ".",
			#make a random point
			p = ogr.CreateGeometryFromWkt('POINT (%f %f)'%(random.uniform(minX,maxX),random.uniform(minY,maxY)))			
			#print p.ExportToWkt()

			# Check if point is in geom... 
			if not geom.Contains(p):
				#print "Not in polygon!!!!!!"
				continue
			
			if min_distance is None or len(return_set) < 1:
				return_set.append(p)
				return_mp.AddGeometry(p)
			else:
				if p.Distance(return_mp) > min_distance:
					return_set.append(p)
					return_mp.AddGeometry(p)
						
		self.pointlist =  return_set
		print ""
		return self.pointlist

	def make_tree_list(self):
		""" Generates a tuple of [ point, dbh, species ] for each tree """
		dbh_func = self.parmlist['dbh_equn']
		leadsp = self.parmlist['leadsp']
		topht = self.parmlist['topht']
		age = self.parmlist['age']
		si = self.parmlist['leadsp_index']
		stkg = self.parmlist['stkg']
		
		
		min_distance = sum(dbh_func(leadsp,topht,age,si,stkg).return_random_variates(n=10) ) / 10 / 100 # Average of some random variates, which is in cm, then to meters...
		
		self.treelist = list()
		
		for p in self.randompointsinpolygon(min_distance=min_distance):
			i = random.randint(0, len(self.spcomp_list)-1)
			sp = self.spcomp_list[i]
			si = self.index_list[sp]
			dbh = dbh_func(sp,topht,age,si,stkg).return_random_variate()
			self.treelist.append((p,dbh,sp))
		
		#self.treelist = [ ( p, dbh_func(leadsp,topht,age,si,stkg).return_random_variate(),random.choice(self.spcomp_list))  for p in self.randompointsinpolygon(min_distance=min_distance) ]
		return self.treelist
	
	def prism_sweep(self,i,ldf_coefficient=0.3535):
		if self.treelist is None: self.make_tree_list()
		return len([ p for p in self.treelist if p[0].Distance(self.stations[i]) < (p[1] * ldf_coefficient  ) ])
		
	def calc_ba(self,ba_factor=2):
		self.calc_ba_value =  sum ( [self.prism_sweep(i) for i in  range(0,10) ] ) * ba_factor
		return self.calc_ba_value

	def make_spcomp_list(self,sppcomp=None):
		if sppcomp is None:
			sppcomp = self.parmlist['spcomp']
		self.spcomp_list = \
			[ 
			item for sublist in 
				[
					[
					self.translate_ON_species_To_ZeligCFS(sppcomp[k:k+3])# species
					for n in range(0,int(sppcomp[k+3:k+6].strip())/10)  # species prop
					] 
					for k in range(0,len(sppcomp),6) 					#Number of species
				]  
				for item in sublist
			]
		#print self.spcomp_list
		return self.spcomp_list
		
	def translate_ON_species_To_ZeligCFS( self, sp ):
		spDict =  \
			{
			'BW':'BOP',
			'WB':'BOP',#Not actually an eFRI species
			'SW':'EPB',
			'SB':'EPN',
			'BF':'SAB',
			'CE':'THO',
			'CW':'THO',
			'PT':'PET',
			'PO':'PET',
			'PJ':'PIG',
			'LA':'MEL',
			'PL':'PEG',
			'PB':'PEB',
			}
		try:
			return spDict[sp.strip().upper()]
		except KeyError:
			return sp.strip().upper()
		
		
	def make_index_list(self):
		topht = self.parmlist['topht']
		age = self.parmlist['age']
		self.index_list =  dict([ (sp,self.parmlist['index'](sp,topht,age,0,0).find_bi()) for sp in set(self.spcomp_list) ])
		return self.index_list
		
	def make_stemsperhectare(self):
		""" 
		Calculates the stemsperhectare by summing  stemsperhectare * spperc by species * ( crown closure / 100 )
		"""
		#print self.spcomp_list
		#print self.index_list
		self.parmlist['stemsperhectare'] = sum ([ self.parmlist['index'](self.spcomp_list[i],self.parmlist['topht'],self.parmlist['age'],self.index_list[self.spcomp_list[i]],0).find_stemsperhectare() for i in range(0,len(self.spcomp_list)) ] ) / len(self.spcomp_list) * ( self.parmlist['cclo'] / 100.0)
		#s = 0
		#for i in range(0,len(self.spcomp_list)):
			#sp = self.spcomp_list[i]
			#ht = self.parmlist['topht']
			#age = self.parmlist['age']
			#index = self.index_list[sp]
			#s = s + self.parmlist['index'](self.spcomp_list[i],self.parmlist['topht'],self.parmlist['age'],self.index_list[self.spcomp_list[i]],0).find_stemsperhectare()
		#self.parmlist['stemsperhectare'] = s
		return self.parmlist['stemsperhectare'] 
		
	def load_plot_to_table(self,connstring='host=server dbname=ff user=angus',srid=3161):
		conn = psycopg2.connect(connstring)
		cur = conn.cursor()
		# Generate ID of centre point 
		cur.execute("SELECT * FROM nextval('synthetic.centrepoints_id_seq')")
		point_id = cur.fetchone()[0]
		
		# Add central point to table of central points
		g = self.parmlist['geom']
		try:
			oi = self.parmlist['id'] #original id
		except KeyError:
			oi = None
		cur.execute("INSERT INTO synthetic.centrepoints ( id, shape, ba, original_id ) VALUES ( %(id)s, ST_SetSRID(ST_Point(%(x)s,%(y)s),%(srid)s), %(ba)s, %(original_id)s )",{'id':point_id, 'x':g.GetX() , 'y':g.GetY(), 'srid':srid, 'ba':self.calc_ba_value, 'original_id':oi })
		
			
		# Add plot boundary to table of plot boundaries
		p = self.bufferedoutline.ExportToWkt()
		cur.execute("INSERT INTO synthetic.plot_outline ( id, shape ) VALUES ( %(id)s, ST_GeomFromText( %(poly)s ,%(srid)s ) )",{'id':point_id, 'poly':p, 'srid':srid })		
		
		# Add plot stations to table of plot stations
		for s in self.stations:
			cur.execute("INSERT INTO synthetic.stations ( plotid, shape ) VALUES ( %(id)s, ST_GeomFromText( %(station)s ,%(srid)s ) )",{'id':point_id, 'station':s.ExportToWkt(), 'srid':srid })		
		
		# Add Trees to table of trees
		for s in self.treelist:
			( t, dbh, species ) = s
			cur.execute("INSERT INTO synthetic.trees ( plotid, shape, dbh, sp ) VALUES ( %(id)s, ST_GeomFromText( %(tree)s ,%(srid)s ), %(dbh)s, %(sp)s )",{'id':point_id, 'tree':t.ExportToWkt(), 'srid':srid, 'dbh':dbh, 'sp':species })
	
	
		conn.commit()

		# Close communication with the database
		cur.close()
		conn.close()
		###########
		# Python/OGR way to set srid
		###########
		#out_srs = ogr.osr.SpatialReference()
		#out_srs.ImportFromEPSG(3161)
		# Assign to geometry
		#point.AssignSpatialReference(out_srs)

		
		
		
		
		
		
		
def oldmain(args):
	"""args is empty right now"""
	sppcomp="Sb  100"
	parmlist = { 
		'sppcomp':sppcomp,
		'oyrorg':1950,
		'stemsperhectare':2200,
		'dbh_equn':zp14_equn(sppcomp[0:2].upper()),
		'leadsp':sppcomp[0:2].upper(),
		'topht':20,
		'age':80,
		'si':3,
		'stkg':1.0
		}
	point = ogr.Geometry(ogr.wkbPoint)
	#print point.ExportToWkt()
	point.AddPoint(623781, 12517404)
	parmlist['geom'] = point
	
	g= generator(parmlist)
	g.test()
	
	g.load_plot_to_table()
	
	return 0

def main(args):
	#parmlist = dict(zip(("spcomp","ht","age","stkg","cclo","wkt"),("Sb  50Bw  30Pt  20",10.5,60,0,70,"POINT(784459.106452826 12471824.4076923)")))
	parmlist = dict(zip(("spcomp","ht","age","stkg","cclo","wkt"),("Sb 100",12.8,80,0,70,"POINT(784459.106452826 12471824.4076923)")))
	parmlist['topht'] = parmlist['ht'] 
	parmlist['geom'] = ogr.CreateGeometryFromWkt(parmlist['wkt'])
	parmlist['dbh_equn'] = zakrewski_penner_equn9() #payandeh_wang_1996
	parmlist['index'] = payandeh_wang_1996
	g= generator(parmlist)
	print g.test()
	#g.load_plot_to_table()
	


if __name__ == '__main__':
	gdal.UseExceptions()
	import sys
	sys.exit(main(sys.argv))
