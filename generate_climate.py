### Generate 48-layer dataset of climate variables from the database
#
#
# 

import psycopg2


sql = """
SELECT %s
FROM 
(SELECT x,y,month from generate_series(1,19) as x, generate_series(1,7) as y, generate_series(1,12) as month ) a
LEFT JOIN 
(
SELECT id, month, x, y, mean_temp, stdev_temp, mean_precip, stdev_precip, 
       st_astext
  FROM climate.monthly_x_y_values
) b ON a.x = b.x AND a.y = b.y AND a.month = b.month
WHERE a.month = %s
ORDER BY a.y, a.x
  """



conn = psycopg2.connect("dbname=ff user=angus password=angus host=server.homenet port=5433 ")
cur = conn.cursor()

#cur.execute(sql)
#r = cur.fetchall()


header = """ncols         19
nrows         7
xllcorner     -95.0
yllcorner     45.0
cellsize      1.0
NODATA_value  -9999.9
"""


for month in range(1,13):
	for metric in [ "mean_temp","stdev_temp","mean_precip","stdev_precip" ]:
		cur.execute(sql%(metric, month) )
		r = cur.fetchall()
		#print "\t"+str(r)
		
		fn = "c:\\temp\\%s_%s.asc"%(metric, month)
		print "Month,metric: %s"%(fn)
		
		f = open(fn,'w')
		f.write(header)
		body = ''
		for p in r:
			if p[0] is None:
				body +=" -9999.9"
			else :
				body +=" "+str(p[0])
			
		v = body.strip()
		f.write(v)
		f.close()
		fn = None
	
band = 0

vrtfn = "c:\\temp\\l.vrt"
f = open(vrtfn,"w")

f.write('<VRTDataset rasterXSize="19" rasterYSize="7">\n')
f.write('\t<GeoTransform>-95.0,  1,  0.0,  52,  0.0, -1</GeoTransform>\n')
for metric in [ "mean_temp","stdev_temp","mean_precip","stdev_precip" ]:
	for month in range(1,13):
	
		band = band + 1
		cur.execute(sql%(metric, month) )
		r = cur.fetchall()		
		fn = "%s_%s.asc"%(metric, month)

		f.write('\t<VRTRasterBand dataType="Float32" band="%s">\n'%(band))
		#f.write('\t\t<NoDataValue>-9999.9</NoDataValue>\n')
		f.write('\t\t<SimpleSource>\n\t\t<SourceFilename relativeToVRT="1">%s</SourceFilename>\n\t\t<SourceBand>%s</SourceBand>\n\t\t<SrcRect xOff="0" yOff="0" xSize="19" ySize="7"/>\n\t\t<DstRect xOff="0" yOff="0" xSize="19" ySize="7"/>\n\t\t</SimpleSource>\n\t</VRTRasterBand>\n'%(fn,1))
		
f.write("</VRTDataset>\n")		


"""
<VRTDataset rasterXSize="512" rasterYSize="512">
  <GeoTransform>440720.0, 60.0, 0.0, 3751320.0, 0.0, -60.0</GeoTransform>
  <VRTRasterBand dataType="Byte" band="1">
    <ColorInterp>Gray</ColorInterp>
    <SimpleSource>
      <SourceFilename relativeToVRT="1">utm.tif</SourceFilename>
      <SourceBand>1</SourceBand>
      <SrcRect xOff="0" yOff="0" xSize="512" ySize="512"/>
      <DstRect xOff="0" yOff="0" xSize="512" ySize="512"/>
    </SimpleSource>
  </VRTRasterBand>
</VRTDataset>


"""
