# AMSIMOD Support Plugin 

AMSIMOD is an application delivered by the Canadian Forest Service. It is
soon to be available publically.

This plugin prepared data for use in the process-based forest growth 
model ZELIG-CFS, distributed by CFS as well.

AMSIMOD is a modelling framework, and ZELIG-CFS is a growth model.

This plugin samples data from forest inventory data which follows the 
Ontario eFRI standard. 

ZELIG-CFS relies on a tree list, and a 
set of ancillary data to estimate growth of trees. This ancillary data 
is weather, location, and tree growth parameters. The file created by 
this QGIS Plugin is a CSV file which AMSIMOD prepares for ZELIG-CFS.

Weather data for this plugin is supplied for default for Ontario at 1-degree
geographic squares. Look in the plugin directory for a sample file.

The weather required is a set of 48 layers - 12 monthly values of each 
of the following:
* Temperature (C)
* Standard deviation of Temperature
* Precipitation (mm)
* Standard deviation of Precipitation

Location of the stand is pulled from a vector data file, using the polygon 
centroid of a forest stand. Since elevation is not usually included in
these files, we poll Google Maps to get a value. If no value can be generated 
from Google Maps, a value of 1000 is used.

The parameters of the stand are used to estimate the number of stems to synthesize. The 
Species Compostion (OSPCOMP), age (OAGE) and height (OHT) are used to estimate
the number of stems and the species mixture of the synthetic trees.

The algorithm used is based on Plonski, as interpreted by Payandeh and Wang (1996).

The diameter of the trees is estimated using Zakrewski and Penner (2014).

### Downloads
* eFRI Technical Specifications https://www.ontario.ca/document/forest-resources-inventory-technical-specifications

## Installation
The simplest method of installation is to use the QGIS Plugin Manager.

You may also download the ZIP version and unzip it in your 
`${HOME}.qgis2\plugins\python` directory. This is fundamentally what the 
plugin manager does.

## Using it
* Find the tree in the toolbar. Or use the vector menu.
* In the dialog that appears, choose your data layer. 
* The default name for the study area is the layer name
* Default values will be picked for plot id and plot number. Any 
non-integer values will be converted to just the digits themselves.
* Plot area defaults to 400m2 for square (20m) or 404.7m2 for circular (11.35m radius)
* Climate data will default to the layer included with the plugin, but
you may choose your own.
* The selection of stands will be used, or all stands
* If you generate a AMSIMOD project, a .pro file will be created.
* If you generate a Zelig-CFS file, a .csv file will be created
* The files will be placed in the directory of the log file specified.

Hit OK and hope it works... :-)

## Sample Data
Sample data will be downloadable soon.

## Advanced Notes
To prep the data from this plugin for ZELIG-CFS directly is to remove leading commas
and replace other commas with spaces. This can be done with 
```bash
cat test.csv | tail -n +2 | sed -r "s/,{1,}/ /g" | sed -e "s/^[ \t]*//" > test.txt
```


Thanks to GeoApt for providing the plugin framework!
(C) 2011-2014 GeoApt LLC - geoapt.com

And thanks to the whole QGIS crew and extended family for providing such
excellent software over the years.
Git revision : $Format:%H$
