# -*- coding: utf-8 -*-
"""
/***************************************************************************
 amsimod_support
								 A QGIS plugin
 Plugin to support AMSIMOD, an application for process-based modelling
							  -------------------
		begin				: 2016-08-21
		git sha			  : $Format:%H$
		copyright			: (C) 2016 by Angus Carr
		email				: angus.carr@gmail.com
 ***************************************************************************/

/***************************************************************************
 *																		 *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or	 *
 *   (at your option) any later version.								   *
 *																		 *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication,QFileInfo,QFile,QIODevice, pyqtSignal
from PyQt4.QtGui import QAction, QIcon, QFileDialog, QMessageBox
from qgis.core import QgsRasterLayer, QgsRaster,QgsPoint,QgsMessageLog,QgsCoordinateTransform, QgsExpression, QgsCoordinateReferenceSystem
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from amsimod_support_dialog import amsimod_supportDialog
import os.path

#import requests
import json
import urllib2


from math import sin, cos, asin, pi, radians


class amsimod_support:
	"""QGIS Plugin Implementation."""
	message_log_file = None
	zelig_file = None
	plot_area_is_default  = True
	parcid_saved = None


	def __init__(self, iface):
		"""Constructor.

		:param iface: An interface instance that will be passed to this class
			which provides the hook by which you can manipulate the QGIS
			application at run time.
		:type iface: QgsInterface
		"""
		# Save reference to the QGIS interface
		self.iface = iface
		# initialize plugin directory
		self.plugin_dir = os.path.dirname(__file__)
		# initialize locale
		locale = QSettings().value('locale/userLocale')[0:2]
		locale_path = os.path.join(
			self.plugin_dir,
			'i18n',
			'amsimod_support_{}.qm'.format(locale))

		if os.path.exists(locale_path):
			self.translator = QTranslator()
			self.translator.load(locale_path)

			if qVersion() > '4.3.3':
				QCoreApplication.installTranslator(self.translator)

		# Create the dialog (after translation) and keep reference
		self.dlg = amsimod_supportDialog()

		# Declare instance attributes
		self.actions = []
		self.menu = self.tr(u'&eFRI Tree List')
		# TODO: We are going to let the user set this up in a future iteration
		self.toolbar = self.iface.addToolBar(u'eFRI Tree List')
		self.toolbar.setObjectName(u'eFRI Tree List')
		
		#Controls connection area
		#find_efrisource_5: On three dots, add a new layer to the map, and select it for the eFRI source...
		#self.dlg.lineEdit.clear()
		self.dlg.sink.setText(r"C:\temp\test.log")  #FIXME - only for debugging
		
		
		#self.dlg.find_efrisource_5.clicked.connect(self.select_input_file)
		self.dlg.find_sink.clicked.connect(self.select_output_file)
		
		self.dlg.plotarea.setField("404.7000122")
		
		#When Layer chosen in efri_source.layerChanged
		#self.dlg.efri_source.layerChanged.connect()
		self.dlg.efri_source.layerChanged.connect(self.set_name_of_study_area )
		self.dlg.efri_source.layerChanged.emit(self.dlg.efri_source.currentLayer())
		
		#Set some defaults for debugging
		self.dlg.plotgroup.setField("POLYID")  #FIXME - only for debugging
		self.dlg.plotid.setField("id")         #FIXME - only for debugging
		self.dlg.sppcomp_expression.setField("OSPCOMP")
		self.dlg.ht_expression.setField("OHT")
		self.dlg.age_expression.setField("OAGE")
		self.dlg.stkg_expression.setField("STKG")
		self.dlg.cclo_expression.setField("OCCLO")
		
		#When the user clicks square plot, and the plot area is the default
		#Set it to 400m^2
		self.dlg.plotshape_Circular.clicked.connect(self.set_plot_area_circular )
		self.dlg.plotshape_Square.clicked.connect(self.set_plot_area_square )
		
		
	# noinspection PyMethodMayBeStatic
	def tr(self, message):
		"""Get the translation for a string using Qt translation API.

		We implement this ourselves since we do not inherit QObject.

		:param message: String for translation.
		:type message: str, QString

		:returns: Translated version of message.
		:rtype: QString
		"""
		# noinspection PyTypeChecker,PyArgumentList,PyCallByClass
		return QCoreApplication.translate('amsimod_support', message)


	def add_action(
		self,
		icon_path,
		text,
		callback,
		enabled_flag=True,
		add_to_menu=True,
		add_to_toolbar=True,
		status_tip=None,
		whats_this=None,
		parent=None):
		"""Add a toolbar icon to the toolbar.

		:param icon_path: Path to the icon for this action. Can be a resource
			path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
		:type icon_path: str

		:param text: Text that should be shown in menu items for this action.
		:type text: str

		:param callback: Function to be called when the action is triggered.
		:type callback: function

		:param enabled_flag: A flag indicating if the action should be enabled
			by default. Defaults to True.
		:type enabled_flag: bool

		:param add_to_menu: Flag indicating whether the action should also
			be added to the menu. Defaults to True.
		:type add_to_menu: bool

		:param add_to_toolbar: Flag indicating whether the action should also
			be added to the toolbar. Defaults to True.
		:type add_to_toolbar: bool

		:param status_tip: Optional text to show in a popup when mouse pointer
			hovers over the action.
		:type status_tip: str

		:param parent: Parent widget for the new action. Defaults None.
		:type parent: QWidget

		:param whats_this: Optional text to show in the status bar when the
			mouse pointer hovers over the action.

		:returns: The action that was created. Note that the action is also
			added to self.actions list.
		:rtype: QAction
		"""

		icon = QIcon(icon_path)
		action = QAction(icon, text, parent)
		action.triggered.connect(callback)
		action.setEnabled(enabled_flag)

		if status_tip is not None:
			action.setStatusTip(status_tip)

		if whats_this is not None:
			action.setWhatsThis(whats_this)

		if add_to_toolbar:
			self.toolbar.addAction(action)

		if add_to_menu:
			self.iface.addPluginToVectorMenu(
				self.menu,
				action)

		self.actions.append(action)

		return action

	def initGui(self):
		"""Create the menu entries and toolbar icons inside the QGIS GUI."""

		icon_path = ':/plugins/amsimod_support/icon.png'
		self.add_action(
			icon_path,
			text=self.tr(u'Generate Tree List from eFRI'),
			callback=self.run,
			parent=self.iface.mainWindow())
		self.add_action(
			icon_path=None,
			add_to_toolbar=False,
			text=self.tr(u'Load Sample eFRI Layer'),
			callback=self.run_load_sample,
			parent=self.iface.mainWindow())



	def unload(self):
		"""Removes the plugin menu item and icon from QGIS GUI."""
		for action in self.actions:
			self.iface.removePluginVectorMenu(
				self.tr(u'&eFRI Tree List'),
				action)
			self.iface.removeToolBarIcon(action)
		# remove the toolbar
		del self.toolbar

###########################################
# functions for operating the GUI
###########################################
	def select_output_file(self):
		"""The user chose the three dots to add a new layer"""
		#Added according to the tutorial... Does nothing useful, yet...
		#Called because we added it to the signal for the ... button
		#Look in the __init__ method 
		filename = QFileDialog.getSaveFileName(self.dlg, "Select output file ","", '*.txt')
		self.dlg.sink.setText(filename)
		
	def set_name_of_study_area(self):
		"""Set_study_area puts the layer name into the study area thing"""
		if self.dlg.efri_source.currentLayer() is not None:
			self.dlg.lineEdit.setText(self.dlg.efri_source.currentLayer().name())
	
	def log(self,msg):
		QgsMessageLog.logMessage(msg, 'eFRI Tree List Generator messages', QgsMessageLog.INFO)
		if self.message_log_file is not None:
			f = QFile(self.message_log_file)
			f.open(QIODevice.Append)
			f.write('%s\n'%msg)
			f.close()

	def set_plot_area_square(self):
		if self.dlg.plotarea.asExpression() == "404.7000122" :
			self.dlg.plotarea.setField("400")

	def set_plot_area_circular(self):
		if self.dlg.plotarea.asExpression() == "400" :
			self.dlg.plotarea.setField("404.7000122")

			
#######################################
# Active Output Functions
#######################################

			
	def generate_line_of_output(self,header,row,sep=","):
		pr = list()
		for c in header:
			try:
				v = row[c]
				pr.append(str(v))
			except KeyError:
				pr.append ('')
		# remove trailing commas...
		return (sep.join(pr))# .rstrip(sep)
			
	def write_zelig(self,dict_of_values,start_file=False):
		"""Write a line to the Zelig File. If start_file, then write the header"""
		# Keep a copy of the ParcId - it gets written every time. We need to keep it around.
		if "ParcId" in dict_of_values.keys():
			self.parcid_saved = dict_of_values["ParcId"]
		else:
			dict_of_values["ParcId"] = self.parcid_saved
			
		cols = ["Aire_observation","ParcNum","ParcId","PlotArea","Plotshape","LAT","LONG_","ALTITUDE","Indep","Simul","NbYears","IntervalPrintout","IntervalDiagnos","Code_QGIS","SolAltAng","DirSolRad","DifSolRad","CanHeight","FieldCap","WiltPoint","SoilFert","TJanvier","TFevrier","TMars","TAvril","TMai","TJuin","TJuillet","TAout","TSeptembre","TOctobre","TNovembre","TDecembre","TSDJanvier","TSDFevrier","TSDMars","TSDAvril","TSDMai","TSDJuin","TSDJuillet","TSDAout","TSDSeptembre","TSDOctobre","TSDNovembre","TSDDecembre","PJanvier","PFevrier","PMars","PAvril","PMai","PJuin","PJuillet","PAout","PSeptembre","POctobre","PNovembre","PDecembre","PSDJanvier","PSDFevrier","PSDMars","PSDAvril","PSDMai","PSDJuin","PSDJuillet","PSDAout","PSDSeptembre","PSDOctobre","PSDNovembre","PSDDecembre","SpecCode","Agemax","dmax","htmax","shapeAdjFact","growthRateCoef","ddmin","ddmax","lightclass","maxdrought","nutriclass","CrownShapeCode","seed","Stocking","SeedInflZone","StressMortProb & tree#","Species","dbh(cm)"]
		if start_file:
			f = QFile(self.zelig_file)
			f.open(QIODevice.WriteOnly)
			msg=','.join(cols)
			f.write('%s\n'%msg)
			f.close()
		else:
			f = QFile(self.zelig_file)
			f.open(QIODevice.Append)
			msg=self.generate_line_of_output(cols,dict_of_values)
			f.write('%s\n'%msg)
			f.close()

	def expression_value(self, expression,feature,layer=None):
		if layer is None:
			layer = self.dlg.efri_source.currentLayer()
		exp = QgsExpression(expression)
		
		if exp.hasParserError():
			raise Exception(exp.parserErrorString())
		exp.prepare(layer.pendingFields())
		value = exp.evaluate(feature)
		if exp.hasEvalError():
			raise ValueError(exp.evalErrorString())

		return value

	def default_species_parameters(self):
		""" Returns a list of species parameters"""
		return [																		
			{	"SpecCode": 1, 	"Agemax": 250, 	"dmax": 150, 	"htmax": 4500, 	"shapeAdjFact": 1.799999952, 	"growthRateCoef": 100, 	"ddmin": 1420, 	"ddmax": 3084, 	"lightclass": 3, 	"maxdrought": 2, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "BOJ", 	},
			{	"SpecCode": 2, 	"Agemax": 140, 	"dmax": 70, 	"htmax": 3000, 	"shapeAdjFact": 1.200000048, 	"growthRateCoef": 160, 	"ddmin": 700, 	"ddmax": 2500, 	"lightclass": 4, 	"maxdrought": 3, 	"nutriclass": 3, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "BOP", 	},
			{	"SpecCode": 3, 	"Agemax": 200, 	"dmax": 64, 	"htmax": 3000, 	"shapeAdjFact": 1, 	"growthRateCoef": 132, 	"ddmin": 447, 	"ddmax": 1929, 	"lightclass": 2, 	"maxdrought": 3, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "EPB", 	},
			{	"SpecCode": 4, 	"Agemax": 400, 	"dmax": 100, 	"htmax": 3500, 	"shapeAdjFact": 1, 	"growthRateCoef": 89, 	"ddmin": 500, 	"ddmax": 2580, 	"lightclass": 1, 	"maxdrought": 2, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0.75, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "EPR", 	},
			{	"SpecCode": 5, 	"Agemax": 50, 	"dmax": 18, 	"htmax": 950, 	"shapeAdjFact": 1, 	"growthRateCoef": 150, 	"ddmin": 889, 	"ddmax": 5500, 	"lightclass": 1, 	"maxdrought": 2, 	"nutriclass": 3, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "ERE", 	},
			{	"SpecCode": 6, 	"Agemax": 75, 	"dmax": 22, 	"htmax": 1500, 	"shapeAdjFact": 1, 	"growthRateCoef": 150, 	"ddmin": 889, 	"ddmax": 5500, 	"lightclass": 1, 	"maxdrought": 2, 	"nutriclass": 3, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "ERP", 	},
			{	"SpecCode": 7, 	"Agemax": 150, 	"dmax": 80, 	"htmax": 3000, 	"shapeAdjFact": 1.200000048, 	"growthRateCoef": 176, 	"ddmin": 1260, 	"ddmax": 6601, 	"lightclass": 2, 	"maxdrought": 3, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "ERR", 	},
			{	"SpecCode": 8, 	"Agemax": 300, 	"dmax": 110, 	"htmax": 4400, 	"shapeAdjFact": 1.700000048, 	"growthRateCoef": 89, 	"ddmin": 1204, 	"ddmax": 3200, 	"lightclass": 1, 	"maxdrought": 2, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "ERS", 	},
			{	"SpecCode": 9, 	"Agemax": 300, 	"dmax": 100, 	"htmax": 4000, 	"shapeAdjFact": 1, 	"growthRateCoef": 75, 	"ddmin": 1225, 	"ddmax": 2279, 	"lightclass": 3, 	"maxdrought": 1, 	"nutriclass": 1, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "FRN", 	},
			{	"SpecCode": 10, 	"Agemax": 366, 	"dmax": 100, 	"htmax": 3700, 	"shapeAdjFact": 1.700000048, 	"growthRateCoef": 72, 	"ddmin": 1327, 	"ddmax": 5556, 	"lightclass": 1, 	"maxdrought": 2, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "HEG", 	},
			{	"SpecCode": 11, 	"Agemax": 650, 	"dmax": 150, 	"htmax": 3500, 	"shapeAdjFact": 1, 	"growthRateCoef": 47, 	"ddmin": 1324, 	"ddmax": 3100, 	"lightclass": 1, 	"maxdrought": 2, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "PRU", 	},
			{	"SpecCode": 12, 	"Agemax": 200, 	"dmax": 65, 	"htmax": 3000, 	"shapeAdjFact": 1, 	"growthRateCoef": 69, 	"ddmin": 250, 	"ddmax": 2404, 	"lightclass": 1, 	"maxdrought": 1, 	"nutriclass": 3, 	"CrownShapeCode": 3, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "SAB", 	},
			{	"SpecCode": 13, 	"Agemax": 50, 	"dmax": 25, 	"htmax": 1100, 	"shapeAdjFact": 1, 	"growthRateCoef": 150, 	"ddmin": 1000, 	"ddmax": 3500, 	"lightclass": 3, 	"maxdrought": 3, 	"nutriclass": 1, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "SOA", 	},
			{	"SpecCode": 14, 	"Agemax": 400, 	"dmax": 100, 	"htmax": 2900, 	"shapeAdjFact": 1.5, 	"growthRateCoef": 55, 	"ddmin": 1000, 	"ddmax": 2188, 	"lightclass": 2, 	"maxdrought": 4, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "THO", 	},
			{	"SpecCode": 15, 	"Agemax": 450, 	"dmax": 150, 	"htmax": 3700, 	"shapeAdjFact": 1, 	"growthRateCoef": 68, 	"ddmin": 1500, 	"ddmax": 3183, 	"lightclass": 4, 	"maxdrought": 2, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "PIB", 	},
			{	"SpecCode": 16, 	"Agemax": 30, 	"dmax": 40, 	"htmax": 2000, 	"shapeAdjFact": 1, 	"growthRateCoef": 200, 	"ddmin": 560, 	"ddmax": 2500, 	"lightclass": 4, 	"maxdrought": 2, 	"nutriclass": 3, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "PRP", 	},
			{	"SpecCode": 17, 	"Agemax": 125, 	"dmax": 75, 	"htmax": 3700, 	"shapeAdjFact": 1, 	"growthRateCoef": 158, 	"ddmin": 889, 	"ddmax": 5556, 	"lightclass": 5, 	"maxdrought": 3, 	"nutriclass": 2, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "PET", 	},
			{	"SpecCode": 18, 	"Agemax": 70, 	"dmax": 75, 	"htmax": 3000, 	"shapeAdjFact": 1, 	"growthRateCoef": 148, 	"ddmin": 1100, 	"ddmax": 3169, 	"lightclass": 5, 	"maxdrought": 3, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "PEG", 	},
			{	"SpecCode": 19, 	"Agemax": 50, 	"dmax": 18, 	"htmax": 1100, 	"shapeAdjFact": 1, 	"growthRateCoef": 158, 	"ddmin": 1928, 	"ddmax": 6011, 	"lightclass": 5, 	"maxdrought": 3, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "COC", 	},
			{	"SpecCode": 20, 	"Agemax": 50, 	"dmax": 18, 	"htmax": 200, 	"shapeAdjFact": 1, 	"growthRateCoef": 158, 	"ddmin": 1928, 	"ddmax": 6011, 	"lightclass": 5, 	"maxdrought": 3, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "VIL", 	},
			{	"SpecCode": 21, 	"Agemax": 250, 	"dmax": 40, 	"htmax": 2000, 	"shapeAdjFact": 1, 	"growthRateCoef": 70, 	"ddmin": 265, 	"ddmax": 1929, 	"lightclass": 2, 	"maxdrought": 3, 	"nutriclass": 3, 	"CrownShapeCode": 4, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "EPN", 	},
			{	"SpecCode": 22, 	"Agemax": 335, 	"dmax": 75, 	"htmax": 2500, 	"shapeAdjFact": 1, 	"growthRateCoef": 66, 	"ddmin": 280, 	"ddmax": 2660, 	"lightclass": 5, 	"maxdrought": 4, 	"nutriclass": 3, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "MEL", 	},
			{	"SpecCode": 23, 	"Agemax": 300, 	"dmax": 100, 	"htmax": 3000, 	"shapeAdjFact": 1.200000048, 	"growthRateCoef": 88, 	"ddmin": 1398, 	"ddmax": 5593, 	"lightclass": 3, 	"maxdrought": 3, 	"nutriclass": 1, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "FRA", 	},
			{	"SpecCode": 24, 	"Agemax": 400, 	"dmax": 100, 	"htmax": 3000, 	"shapeAdjFact": 1, 	"growthRateCoef": 66, 	"ddmin": 1100, 	"ddmax": 4571, 	"lightclass": 3, 	"maxdrought": 2, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "CHR", 	},
			{	"SpecCode": 25, 	"Agemax": 250, 	"dmax": 25, 	"htmax": 1000, 	"shapeAdjFact": 1, 	"growthRateCoef": 37.04, 	"ddmin": 1007, 	"ddmax": 2880, 	"lightclass": 3, 	"maxdrought": 2, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "BOG", 	},
			{	"SpecCode": 26, 	"Agemax": 200, 	"dmax": 100, 	"htmax": 3000, 	"shapeAdjFact": 2, 	"growthRateCoef": 132.1, 	"ddmin": 2132, 	"ddmax": 5993, 	"lightclass": 4, 	"maxdrought": 3, 	"nutriclass": 1, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "CET", 	},
			{	"SpecCode": 27, 	"Agemax": 100, 	"dmax": 50, 	"htmax": 1500, 	"shapeAdjFact": 1, 	"growthRateCoef": 137.7, 	"ddmin": 1278, 	"ddmax": 5556, 	"lightclass": 1, 	"maxdrought": 3, 	"nutriclass": 2, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "OSV", 	},
			{	"SpecCode": 28, 	"Agemax": 125, 	"dmax": 120, 	"htmax": 3000, 	"shapeAdjFact": 1, 	"growthRateCoef": 212, 	"ddmin": 1500, 	"ddmax": 4700, 	"lightclass": 2, 	"maxdrought": 1, 	"nutriclass": 3, 	"CrownShapeCode": 2, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.368999988, 	"Species": "ERA", 	},
			{	"SpecCode": 29, 	"Agemax": 150, 	"dmax": 50, 	"htmax": 2500, 	"shapeAdjFact": 1, 	"growthRateCoef": 146, 	"ddmin": 213, 	"ddmax": 2234, 	"lightclass": 5, 	"maxdrought": 4, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "PIG", 	},
			{	"SpecCode": 30, 	"Agemax": 310, 	"dmax": 75, 	"htmax": 2500, 	"shapeAdjFact": 1, 	"growthRateCoef": 71, 	"ddmin": 877, 	"ddmax": 2053, 	"lightclass": 4, 	"maxdrought": 4, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "PIR", 	},
			{	"SpecCode": 31, 	"Agemax": 150, 	"dmax": 75, 	"htmax": 2500, 	"shapeAdjFact": 1, 	"growthRateCoef": 148, 	"ddmin": 555, 	"ddmax": 2491, 	"lightclass": 5, 	"maxdrought": 1, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "PEB", 	},
			{	"SpecCode": 32, 	"Agemax": 400, 	"dmax": 100, 	"htmax": 3500, 	"shapeAdjFact": 1, 	"growthRateCoef": 76, 	"ddmin": 1600, 	"ddmax": 5556, 	"lightclass": 3, 	"maxdrought": 3, 	"nutriclass": 2, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "CHB", 	},
			{	"SpecCode": 33, 	"Agemax": 300, 	"dmax": 89, 	"htmax": 2500, 	"shapeAdjFact": 1, 	"growthRateCoef": 74, 	"ddmin": 1700, 	"ddmax": 5133, 	"lightclass": 3, 	"maxdrought": 4, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "CHG", 	},
			{	"SpecCode": 34, 	"Agemax": 300, 	"dmax": 100, 	"htmax": 3000, 	"shapeAdjFact": 1, 	"growthRateCoef": 88, 	"ddmin": 1800, 	"ddmax": 5200, 	"lightclass": 3, 	"maxdrought": 3, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "CHN", 	},
			{	"SpecCode": 35, 	"Agemax": 200, 	"dmax": 75, 	"htmax": 2500, 	"shapeAdjFact": 1, 	"growthRateCoef": 111, 	"ddmin": 2217, 	"ddmax": 5153, 	"lightclass": 3, 	"maxdrought": 1, 	"nutriclass": 3, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "CHP", 	},
			{	"SpecCode": 36, 	"Agemax": 140, 	"dmax": 100, 	"htmax": 3000, 	"shapeAdjFact": 1, 	"growthRateCoef": 189, 	"ddmin": 1207, 	"ddmax": 3155, 	"lightclass": 2, 	"maxdrought": 2, 	"nutriclass": 1, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "TIL", 	},
			{	"SpecCode": 37, 	"Agemax": 300, 	"dmax": 80, 	"htmax": 2500, 	"shapeAdjFact": 1, 	"growthRateCoef": 74, 	"ddmin": 1504, 	"ddmax": 6200, 	"lightclass": 3, 	"maxdrought": 3, 	"nutriclass": 2, 	"CrownShapeCode": 1, 	"seed": 0, 	"Stocking": 0, 	"SeedInflZone": 200, 	"StressMortProb & tree#": 0.369, 	"Species": "ORA", 	},
			{	"SpecCode": -999, 	"Agemax": -999, 	"dmax": -999, 	"htmax": -999, 	"shapeAdjFact": -999, 	"growthRateCoef": -999, 	"ddmin": -999, 	"ddmax": -999, 	"lightclass": -999, 	"maxdrought": -999, 	"nutriclass": -999, 	"CrownShapeCode": -999, 	"seed": -999, 	"Stocking": -999, 	"SeedInflZone": -999, 	"StressMortProb & tree#": -999, 	"Species": "zzz", 	},
		]																		





	def run(self):
		"""Run method that performs all the real work"""
		# show the dialog
		self.dlg.show()
		# Run the dialog event loop
		result = self.dlg.exec_()
		# See if OK was pressed
		if result:
			############################################################
			# Extract dialog info
			############################################################
			#Get the generic output filename- used for a log file
			import os
			of_log = self.dlg.sink.text()
			self.message_log_file = of_log
			self.log("Logging Messages to %s"%of_log)
			of_dir = os.path.dirname(of_log)
			self.log("Saving files to %s"%of_dir)
			
			of_zelig = '.'.join([os.path.splitext(of_log)[0],'csv'])
			self.zelig_file = of_zelig
			self.log("Zelig file is %s"%of_zelig)

			of_amsimod = '.'.join([os.path.splitext(of_log)[0],'pro'])
			self.amsimod_file = of_amsimod
			self.log("AMSIMOD file is %s"%of_amsimod)
			
			############################################################
			# Find input files (vector and climate raster)
			############################################################
			#Get Input Layer
			frilayer = self.dlg.efri_source.currentLayer()
			fricrs = frilayer.crs()

			#Report on the list of fields
			#self.log ( ','.join([ field.name() for field in  frilayer.pendingFields() ])  )
			
			#Grab/Create Climate Layer
			if self.dlg.climate_default.isChecked() :
				climateFileName = os.path.join(self.plugin_dir , "default_climate.tif")
				fileInfo = QFileInfo(climateFileName)
				filepath = fileInfo.path()
				baseName = fileInfo.baseName()
				rlayer = QgsRasterLayer(climateFileName, baseName)
				if not rlayer.isValid():
				  self.log( "Climate Raster Layer failed to load! %s"%(climateFileName))
				else:
					self.log("Loaded Raster %s"%(climateFileName))
				rcrs = rlayer.crs()
				#rcrs = QgsCoordinateReferenceSystem(4326)
			else :
				rlayer = self.dlg.climate_layer.currentLayer()
				if not rlayer.isValid():
				  self.log( "Climate Raster Layer failed to load!")
				else:
					self.log("Loaded Raster")
				rcrs = rlayer.crs()
				
				#rcrs = QgsCoordinateReferenceSystem(4326)
			
			geo_crs = QgsCoordinateReferenceSystem(4326)
			xform = QgsCoordinateTransform(fricrs, rcrs) 
			geo_xform = QgsCoordinateTransform(fricrs, geo_crs)
			
			############################################################
			# Generate header data, using the first feature
			############################################################
			feature0 = None
			listOfFeatures = None
			#Get "Climate Centroid" - for now, centroid of first stand
			if self.dlg.stands_from_selection.isChecked():
				self.log("getting stand from selection")
				if len(frilayer.selectedFeatures()) > 0:
					listOfFeatures = frilayer.selectedFeatures()
				else :
					listOfFeatures = frilayer.getFeatures()
			elif self.dlg.stands_all.isChecked():
				self.log("getting stand from all")
				listOfFeatures =  frilayer.getFeatures()
						
			climate_keys = ["TJanvier","TFevrier","TMars","TAvril","TMai","TJuin","TJuillet","TAout","TSeptembre","TOctobre","TNovembre","TDecembre",
					"TSDJanvier","TSDFevrier","TSDMars","TSDAvril","TSDMai","TSDJuin","TSDJuillet","TSDAout","TSDSeptembre","TSDOctobre","TSDNovembre","TSDDecembre",
					"PJanvier","PFevrier","PMars","PAvril","PMai","PJuin","PJuillet","PAout","PSeptembre","POctobre","PNovembre","PDecembre",
					"PSDJanvier","PSDFevrier","PSDMars","PSDAvril","PSDMai","PSDJuin","PSDJuillet","PSDAout","PSDSeptembre","PSDOctobre","PSDNovembre","PSDDecembre" ]
			
			#Generate ZeligCFS Input File
			#Write Line 1 - header
			self.write_zelig(dict(),start_file=True)


			############################################################
			# Iterate through stands
			############################################################

			#Generate some stands
			from generator import generator
			from zakrewskiPenner2014 import zp14_equn, zakrewski_penner_equn9
			from payandeh_wang_1996 import payandeh_wang_1996
			from ecosite_lut import ecosite_lut
			
			# Uses an overloaded column number to be confusing... "StressMortProb & tree#","Species","dbh(cm)"
			for f in  listOfFeatures :  
				feature0=f
				
				#Write Line 2 - Plot info - group and id should both be numbers only...
				study_area = (self.dlg.lineEdit.text().replace(' ','_'))[:21]
				plotgroup =	''.join(c for c in str(self.expression_value( expression= self.dlg.plotgroup.asExpression(), feature=feature0 )) if c.isdigit())
				plotid = 	''.join(c for c in str(self.expression_value( self.dlg.plotid.asExpression(), feature0 )) if c.isdigit())

				if self.dlg.plotshape_Circular.isChecked() :
					plotshape = 'circular'
					plotshape_num = 1
				elif self.dlg.plotshape_Square.isChecked() :
					plotshape = 'square'
					plotshape_num = 2
				plotarea = self.expression_value( expression= self.dlg.plotarea.asExpression(), feature=feature0 )
				
				self.write_zelig({"Aire_observation":study_area,"ParcNum":plotgroup,"ParcId":plotid,"PlotArea":plotarea,"Plotshape":plotshape_num})
				
				#Write Line 3 - Simulation Info
				#FIXME - where do we get this info?
				self.write_zelig({"Indep":0,"Simul":1,"NbYears":200,"IntervalPrintout":5,"IntervalDiagnos":5,"Code_QGIS":0})
				
				#Get Lat/Lon/Alt from shape
				#FIXME - Flag values should come from the data...
				feature_wgs84 = geo_xform.transform(feature0.geometry().centroid().asPoint())
				latitude = feature_wgs84.y()
				longitude = feature_wgs84.x()
				altitude = 1000
				
				#import requests
				#FIXME:if this fails, don't try again
				try:
					url = "https://maps.googleapis.com/maps/api/elevation/json?locations=%s,%s&key"%(latitude,longitude)
					#self.log("URL For Elevation Request: %s"%(url))
					r = json.load( urllib2.urlopen(url) ) #requests.get(url).json()
					altitude =  int(r["results"][0]["elevation"])
				except Exception as e:
					self.log("Elevation Request failed: %s"%(e))
					altitude = 1000
				
				self.log("Feature ID %d: %s %s %s" % (feature0.id(),longitude, latitude, altitude))


				
				#Write Line 4 - lat/long/alt
				self.write_zelig({"LAT":latitude,"LONG_":longitude,"ALTITUDE":altitude})
				
				#Write Line 5 - Solar Data
				#FIXME Using flag values is bad
				#The altitude angle is calculated as follows:  
				# sin (Al) = [cos (L) * cos (D) * cos (H)] + [sin (L) * sin (D)]  
				# where:  
				#	 Al = Solar altitude angle 
				#	 L = Latitude (negative for Southern Hemisphere) 
				#	 D = Declination (negative for Southern Hemisphere) 
				#	 H = Hour angle  
				D = radians(23.45) #Summer Solstice Solar Altitude Angle
				H = pi / 2.0 
				L = radians( latitude )
				Al = asin(   (cos (L) * cos (D) * cos (H)) + (sin (L) * sin (D))   )
				
				
				#Or... from http://www.pveducation.org/pvcdrom/properties-of-sunlight/elevation-angle
				Al = (pi/2.0) + L - D 
				
				self.write_zelig({"SolAltAng":Al,"DirSolRad":0.5,"DifSolRad":0.5,"CanHeight":1})
				
				#Write Line 6 - Soil Data
				#FIXME Flag Values
				try:
					ecosite = feature0.attribute( 'pri_eco' )
				except KeyError:
					ecosite = feature0.attribute( 'PRI_ECO' )
				self.write_zelig(ecosite_lut(ecosite,'FieldCap,WiltPoint,SoilFert'))
				
				#self.write_zelig({"FieldCap":27,"WiltPoint":15,"SoilFert":10})

				#Write Line 7 - Climate Data (TEMP, SD of Temp, PRCP, SD of PRCP):12
				ident = rlayer.dataProvider().identify(feature_wgs84, QgsRaster.IdentifyFormatValue)
				
				#if ident.isValid():
				#	self.log(str(ident.results()))
				
				climate_values =    map ( lambda i : round(ident.results()[i],1) if  (i < 13 ) or ( (i > 24) and (i<= 37)) else ident.results()[i]  , range(1,49 ) ) #1,13 and 25,37 should be rounded to 1 decimal place
				self.write_zelig(dict(zip(climate_keys,climate_values)))
				
				#Write Line 8+ - Species Parameters
				for sp_param in self.default_species_parameters():	self.write_zelig(sp_param)

							
				#Modify to iterate through plots in selection... 
				spcomp = self.expression_value( expression= self.dlg.sppcomp_expression.asExpression(), feature=feature0 ) # feature0.attribute( 'ospcomp' )
				ht = self.expression_value( expression= self.dlg.ht_expression.asExpression(), feature=feature0 ) # feature0.attribute( 'oht' )
				age = self.expression_value( expression= self.dlg.age_expression.asExpression(), feature=feature0 ) #feature0.attribute( 'oage' )
				stkg = self.expression_value( expression= self.dlg.stkg_expression.asExpression(), feature=feature0 ) #feature0.attribute( 'stkg' )
				cclo = self.expression_value( expression= self.dlg.cclo_expression.asExpression(), feature=feature0 ) #feature0.attribute( 'occlo' )
				wkt = feature0.geometry().centroid().asPoint().wellKnownText() #"POINT(784459.106452826 12471824.4076923)"
				
				# Generate the trees and put them in the CSV File
				parmlist = dict(zip(("spcomp","ht","age","stkg","cclo","wkt"),(spcomp,ht,age,stkg,cclo,wkt)))
				parmlist['topht'] = parmlist['ht'] 
				#parmlist['geom'] = ogr.CreateGeometryFromWkt(parmlist['wkt'])
				parmlist['plot_shape'] = plotshape
				parmlist['plot_area'] = plotarea
				
				parmlist['dbh_equn'] = payandeh_wang_1996 # zakrewski_penner_equn9() 
				parmlist['index'] = payandeh_wang_1996
				g= generator(parmlist)
				self.log(g.test())
				#g.make_tree_list()
				
				self.log("Creating %s trees"%(len(g.treelist)))
				
				#Write Line 46+ - Tree list
				i=1
				for (p,dbh,sp) in g.treelist:
					#self.log("(p,dbh,sp): %s, %s, %s"%(p,dbh,sp))
					self.write_zelig({"StressMortProb & tree#":i,"Species":sp,"dbh(cm)":round(dbh,1)})
					i=i+1
				self.write_zelig({"StressMortProb & tree#":-999,"Species":"zzz","dbh(cm)":-999})
			
				#Write Line 204+ - "Edouard_65_code1.txt"
				#Write a dummy text value, as required... Another overloaded column value
				# self.write_zelig({"Species":study_area + "_" + plotgroup + ".txt" }) # Apparently not needed - see #11


			############################################################
			# Set up AMSIMOD Project File
			############################################################
			#Generate AMSIMOD Project File
			if self.dlg.generate_amsimod.isChecked() :
				with open(self.amsimod_file, "w") as text_file:
					text_file.write("#####Simulation#####\n")
					text_file.write("Simulation1\n")
					text_file.write(os.path.join(os.path.abspath(self.plugin_dir),r'ZELIG_37.exe'))
					text_file.write("\n")
					text_file.write( self.zelig_file )
					text_file.write("\n")
					text_file.write("#######Inter########\n")
					text_file.write("#######OutPut#######\n")
					text_file.write("tree_dbh.txt\n")
					text_file.write("basal_area.txt\n")
					text_file.write("density.txt\n")
					text_file.write("plotinfo.txt\n")
					text_file.write("QGIS_output.txt\n")
					
				#Alert User - Removed to comply with not having arch-specific binaries in QGIS Plugin
				#mb = QMessageBox()
				#mb.setIcon(QMessageBox.Question)
				#mb.setText('Launch AMSIMOD?')
				#mb.setInformativeText('AMSIMOD export is complete')
				#mb.setStandardButtons(QMessageBox.Open | QMessageBox.No )
				#mb.buttonClicked.connect(self.msgbtn)
				#retval = mb.exec_()
				##self.log( "value of pressed message box button: %s"%(retval))
	def run_load_sample(self):
		layer = self.iface.addVectorLayer(os.path.join(self.plugin_dir,"sample_efri.geojson"), "Sample Data", "ogr")
		if not layer:
		  print "Layer failed to load!"
		
	def msgbtn(self,i):
		#self.log( "Button pressed is: %s"%(i.text()))
		#pass
		if i.text() == 'Open':
			import subprocess
			od_amsimod = os.path.dirname(self.amsimod_file)
			self.log(os.path.join(os.path.dirname(os.path.abspath(__file__)),r'AMSIMOD_ZELIG-CFS\AMSIMOD.exe'))
			self.log("AMSIMOD CWD Folder is %s"%od_amsimod)
			#try:
			subprocess.Popen([os.path.join(os.path.dirname(os.path.abspath(__file__)),r'AMSIMOD_ZELIG-CFS\AMSIMOD.exe')],cwd=od_amsimod)
			#except WindowsError:
			#	pass #ignore when it can't find it. Silence is ok for now, until we can handle it better. #FIXME
			


