#import rpy2.robjects as robjects
from math import exp,log
"""This file contains equn7 and equn9 from Zakrewski and Penner, 2014 (zp14)

Use it by:
	e=best_equn('CE')('CE',10,50,1,1)
	print e.return_random_variates(7)

This example picks the class with the best fit equation according to zp14 by leading species
parameters are: leadsp,topht,age,si,stkg

Equation 9 only uses the leadsp and topht, but accepts the rest for the sake of coding simplicity...

"""

import random

class Wiebull_equation():
#	def return_r_random_variates(self, n=1):
#		return robjects.r('rweibull(%d, shape = %f, scale=%f)'%(n,self.b2,self.b3))
#	def return_r_random_variate(self):
#		return robjects.r('rweibull(%d, shape = %f, scale=%f)'%(1,self.b2,self.b3))[0]
		
	def return_py_random_variates(self,n=1):
		return [ random.weibullvariate(alpha=self.b2,beta=self.b3) for n in range(0,n+1) ]
	def return_py_random_variate(self):
		return random.weibullvariate(alpha=self.b2,beta=self.b3)

	def return_random_variates(self,n=1):
		return [ random.weibullvariate(alpha=self.b2,beta=self.b3) for n in range(0,n+1) ]
	def return_random_variate(self):
		return random.weibullvariate(alpha=self.b2,beta=self.b3)
	
	def duplicate_species(self,kv={\
		'PT':'PO','PET':'PO','PEB':'PO','PEG':'PO','PB':'PO',
		'SW':'SB','EPN':'SB','EPB':'SB',
		'AB':'SB',
		'MEL':'LA',
		'SAB':'BF',
		'BOP':'BW',
		'CW':'CE',
		'THO':'CE',
		'PIG':'PJ',
		}):
		for k,v in kv.iteritems() :
			self.A1[k]=self.A1[v]
			self.A2[k]=self.A2[v]
			self.B2[k]=self.B2[v]
			self.B3[k]=self.B3[v]

class equn7(Wiebull_equation):
	# leadsp: [ intercept, topht,age,si,stkg]
	A1={'PJ':	[0.364326,	1.022103,	0,	0,	0], 
		'SB':	[1.828285,	0.519163,	0,	0,	0],
		'BF':	[0.21753,	       0,	0.405587,	0.34424,0], 
		'CE':	[2.012547,	0.398275,	0,	0,	0],
		'LA':	[       0,	1.156371,	0,	0,	0],
		'BW':	[2.536168,	0.256949,	0,	0,	0],
		'PO':	[1.014267,	0.804527,	0,	0,	0]
		}
	# leadsp: [ intercept, topht,age,si,stkg]
	A2={'PJ':[-0.44194,0.966693,0,0,0],
		'SB':[-0.44194,0.966693,0,0,0],
		'BF':[1.466265,0.285012,0,0,-0.11232],
		'CE':[2.333677,0,0,0,0],
		'LA':[0,0.834132,0,0,0],
		'BW':[2.273453,0,0,0,0],
		'PO':[0,0,0.118592,0.734036,0]
		}
	# leadsp: [ intercept,topht,age,si,stkg,a1,a2,HSI]
	B2={'PJ':[0.596372,0.639939,0,0,0,0,0,0],
		'SB':[-0.38137,0.697139,0,0,0,0,0,0],
		'BF':[0,0,0,0,0,0,0,0.435516],
		'CE':[-4.78716,2.478397,0,0,0,0,0,0],
		'LA':[0,0.693773,0,0,0,0,0,0],
		'BW':[0,0.723003,0,0,0,0,0,0],
		'PO':[2.250925,0,0,0,-0.41276,0,0,0]
		}
	# leadsp: [ intercept,topht,age,si,stkg,a1,a2,HSI]
	B3={'PJ':[-0.83124,1.224535,0,0,0,0,0,0],
		'SB':[-1.3222,1.423447,0,0,0,0,0,0],
		'BF':[-0.92522,1.295468,0,0,0,0,0,0],
		'CE':[-0.76593,1.228698,0,0,0,0,0,0],
		'LA':[-0.90383,1.253421,0,0,0,0,0,0],
		'BW':[-0.43916,1.111328,0,0,0,0,0,0],
		'PO':[-1.33818,0,0,0.375959,0.0277,0,1.260882,0]
		}
	
	a1 = None
	a2 = None
	b2 = None
	b3 = None
	HSI = None
	
	def __init__(self,leadsp,topht,age,si,stkg):
		self.duplicate_species()
		self.alpha1(leadsp,topht,age,si,stkg)
		self.alpha2(leadsp,topht,age,si,stkg)
		self.HSI = pow(self.a1, 2) / self.a2
		self.weibshape(leadsp,topht,age,si,stkg)
		self.weibscale(leadsp,topht,age,si,stkg)
		
	def alpha1(self,leadsp,topht,age,si,stkg):
		(a11,a12,a13,a14,a15) = self.A1[leadsp]
		self.a1 =  exp(a11 + a12 * log(topht) + a13 * log(age) + a14 * log(si)) + a15 * log(stkg)
		return self.a1
	
	def alpha2(self,leadsp,topht,age,si,stkg):
		(a21,a22,a23,a24,a25) = self.A2[leadsp]
		self.a2 = exp(a21 + a22 * log(topht) + a23 * log(age) + a24 * log(si)) + a25 * log(stkg)
		return self.a2 
	
	def weibshape(self,leadsp,topht,age,si,stkg):
		(b21,b22,b23,b24,b25,b26,b27,b28) = self.B2[leadsp]
		self.b2 = exp(b21 + b22 * log(topht) + b23 * log(age) + b24 * log(si)) + b25 * log(stkg) + b26 * log(self.a1) + b27 * log(self.a2) + b28 * log (self.HSI)
		return self.b2 
	
	def weibscale(self,leadsp,topht,age,si,stkg):
		(b31,b32,b33,b34,b35,b36,b37,b38) = self.B3[leadsp]
		self.b3 = exp(b31 + b32 * log(topht) + b33 * log(age) + b34 * log(si)) + b35 * log(stkg) + b36 * log(self.a1) + b37 * log(self.a2) + b38 * log (self.HSI)
		return self.b3 
	

class equn9(Wiebull_equation):
	A1 = { 'PJ':[0.364326,1.022103],
		'SB':[1.828285 , 0.519163 ],
		'BF':[ 1.671952, 0.569095],		
		'CE':[ 2.012547, 0.398275],
		'LA':[ 0, 1.156371],
		'BW':[ 2.536168, 0.256949],
		'PO':[ 1.185542, 0.747551]
		}
	A2 = { 'PJ':[-0.44194,0.966693],
		'SB':[ 2.108934, 0.077811],
		'BF':[ 1.676742, 0.223038],
		'CE':[ 2.333677, 0],
		'LA':[ 0, 0.834132],
		'BW':[ 2.273453,0 ],
		'PO':[ 0, 0.806795]
		}
	B2 = { 'PJ':[0.596372,0.639939],
		'SB':[ -0.38137, 0.807225],
		'BF':[ 0.770924, 0.353886],
		'CE':[ -4.78716, 2.478397],
		'LA':[ 0, 0.693773],
		'BW':[ 0, 0.723003 ],
		'PO':[ -0.78417, 1.040414]
		}
	B3 = { 'PJ':[-0.83124,1.224535],
		'SB':[ -1.3222, 1.423447],
		'BF':[ -0.97673, 1.316139],
		'CE':[ -0.76593, 1.228698],
		'LA':[ -0.90383, 1.253421],
		'BW':[ -0.43916, 1.111328],
		'PO':[ -0.63331, 1.164609]
		}
	a1 = None
	a2 = None
	b2 = None
	b3 = None
	
	def __init__(self,leadsp,topht):
		self.duplicate_species()
		self.alpha1(leadsp,topht)
		self.alpha2(leadsp,topht)
		self.weibshape(leadsp,topht)
		self.weibscale(leadsp,topht)
		

	def __init__(self,leadsp,topht,age,si,stkg):
		self.duplicate_species()
		self.alpha1(leadsp,topht)
		self.alpha2(leadsp,topht)
		self.weibshape(leadsp,topht)
		self.weibscale(leadsp,topht)		


		
	def alpha1(self,leadsp,topht):
		(a11,a12) = self.A1[leadsp]
		self.a1 =  exp(a11 + a12 * log(topht))
		return self.a1
	
	def alpha2(self,leadsp,topht):
		(a21,a22) = self.A2[leadsp]
		self.a2 = exp(a21 + a22 * log(topht))
		return self.a2 
	
	def weibshape(self,leadsp,topht):
		(b21,b22) = self.B2[leadsp]
		self.b2 = exp(b21 + b22 * log(topht))
		return self.b2 
	
	def weibscale(self,leadsp,topht):
		(b31,b32) = self.B3[leadsp]
		self.b3 = exp(b31 + b32 * log(topht))
		return self.b3 

def zakrewski_penner_equn9():
	return equn9
	
def zp14_equn(leadsp):
	best = {'PJ':equn9,
			'SB':equn9,
			'BF':equn7,
			'CE':equn9,
			'LA':equn7,
			'BW':equn9,
			'PO':equn7
			}
	return best[leadsp.upper()]
	

if __name__ == '__main__':
	#print robjects.r['pi'][0]

	s = 'rweibull(7, shape=2.71828, scale=100)'
	#print robjects.r(s)
	
	e = equn7('PJ',10,50,1,1)
	print e.alpha1('PJ',10,50,1,1)
	print e.return_random_variates(7)
	

	e = equn7('BF',10,50,1,1)
	print e.return_random_variates(7)
	
	e=zp14_equn('CE')('CE',10,50,1,1)
	print e.return_random_variates(7)
	print e.return_random_variate()
	
	


